.onLoad <- function(libname, pkgname) {
  require("methods", character=TRUE, quietly=TRUE)  # Required to eliminate error in check (http://lists.r-forge.r-project.org/pipermail/rcpp-core/2011-June/000449.html)
  loadRcppModules()

  setOldClass("hclust")  # Specify "type" of S3 class for use in S4 class fields

  # Class encapsulating co-expression analysis results (unfortunately not well supported by Roxygen2, so manually documented)
  setClass(
    Class="CoexppClusters",
    representation=representation(data="matrix", clusters="hclust", wgcna="Rcpp_Wgcna", beta="numeric", sftStatistics="data.frame")
  )

  if (!isGeneric("exprs", where=topenv(parent.frame()))) {
    if (exists("exprs") && is.function(exprs))
      fn <- exprs
    else
      fn <- function(x) { standardGeneric("exprs") }
    setGeneric("exprs", fn)
  }
  setMethod("exprs", "CoexppClusters", function(x){ x@data })

  setMethod("length", "CoexppClusters", function(x) { x@wgcna$observation_count() })
}

CoexppClusters <- function(data, clusters, wgcna, beta, sftStatistics) {
  new(Class="CoexppClusters", data=data, clusters=clusters, wgcna=wgcna, beta=beta, sftStatistics=sftStatistics)
}

.working2tom <- function(tm, labels=NULL) {
  tm[lower.tri(tm)] <- t(tm)[lower.tri(tm)]
  diag(tm) <- 0.
  if (!is.null(labels))
    dimnames(tm) <- list(labels, labels)
  tm
}

.working2adj <- function(aj, labels=NULL) {
  aj[upper.tri(aj)] <- t(aj)[upper.tri(aj)]
  diag(aj) <- 0.
  if (!is.null(labels))
    dimnames(aj) <- list(labels, labels)
  aj
}

clusters <- function(x) { x@clusters }
tom <- function(x) { .working2tom(x@wgcna$working_matrix(), colnames(x@data)) }
adj <- function(x) { .working2adj(x@wgcna$working_matrix(), colnames(x@data)) }
connectivity <- function(x) { x@wgcna$connectivity() }

sampleMatrix <- function(x, samples, kind=c("adj", "tom")) { 
  kind = match.arg(kind)
  r <- switch(kind,
    adj = x@wgcna$sampleAdjMatrix(samples),
    tom = x@wgcna$sampleTOMMatrix(samples),
    stop(sprintf("Matrix kind %s not supported", kind))
  )
  diag(r) <- 0.
  dimnames(r) <- rep(list(colnames(x@data)[samples]),2)
  invisible(r)
}

moduleStatistics <- function(x, kind=c("k", "kByModule", "sumTOM", "sumTOMByModule", "ccByModule"), modules=NULL, moduleCounts=NULL) {
  kind = match.arg(kind)
  switch(kind,
    k = x@wgcna$connectivity(),
    kByModule = x@wgcna$k_by_module(modules),
    sumTOM = x@wgcna$sumTOM(),
    sumTOMByModule = x@wgcna$sumTOM_by_module(modules),
    ccByModule = x@wgcna$cc_by_module(modules, moduleCounts),
    stop(sprintf("Statistic kind %s not supported", kind))
  )
}
