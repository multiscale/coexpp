#ifndef COEXPP_WGCNA_H
#define COEXPP_WGCNA_H

#include <cmath>
#include <limits>

#include <iostream>
#include <fstream>
#include <string>
using namespace std;

//Version 8/24/2012
#define FILE_VERSION 8242012

namespace Coexpp {
  using namespace Eigen;

  template<class DataScalar, class DistanceScalar=DataScalar>
  class WGCNA {
    public:

      typedef DistanceScalar distance_scalar_type;
      typedef Map<Matrix<DataScalar, Dynamic, Dynamic> > data_matrix_type;

      typedef Matrix<DistanceScalar, Dynamic, Dynamic> distance_matrix_type;
      typedef Matrix<DistanceScalar, 1, Dynamic>       distance_row_matrix_type;
      typedef Matrix<DistanceScalar, Dynamic, 1>       distance_vector_type;
      typedef TriangularView<distance_matrix_type, Eigen::StrictlyLower> adjacency_matrix_type;
      typedef TriangularView<distance_matrix_type, Eigen::StrictlyUpper> dissimilarity_matrix_type;

      enum MatrixContents {
        UNINITIALIZED,
        CORRELATION,
        ADJACENCY,
        DISSIMILARITY,
        CONNECTIVITY
      };


      WGCNA(const data_matrix_type& data_) :
        data(data_), 
        working_matrix( data.cols(), data.cols() ),
        adjacency_matrix( working_matrix.template triangularView<StrictlyLower>() ),
        dissimilarity_matrix( working_matrix.template triangularView<StrictlyUpper>() ),
        connectivity(data.cols()),
        adjacency_function(PowerAdjacency(1.)),  // Set default adjancency
        lower_contents(UNINITIALIZED),
        upper_contents(UNINITIALIZED),
        connectivity_contents(UNINITIALIZED)
      {
      }

    private:
      
      WGCNA();

    public:

      class PowerAdjacency : public std::unary_function<DistanceScalar, DistanceScalar> {
        public:

          PowerAdjacency(DistanceScalar beta_, bool narm_=false) : beta(beta_), narm(narm_) {}
        
          DistanceScalar operator()(const DistanceScalar& v) const { 
            return (narm && std::isnan(v)) ? 0. : std::pow( std::abs( v ), beta ); 
          }
          
          DistanceScalar GetBeta() {
            return beta;
          }
          
          bool GetNarm() {
            return narm;
          }

        private:

          DistanceScalar beta;
          bool narm;
      };


    public:

      ssize_t observation_count() const { return data.cols(); }

      const data_matrix_type& get_data_matrix() const { return data; }
      const distance_matrix_type& get_working_matrix() const { return working_matrix; }
      
      const adjacency_matrix_type&  get_adjacency_matrix() const { return adjacency_matrix; }
      adjacency_matrix_type& get_adjacency_matrix() { return adjacency_matrix; }
      
      template <class Derived>
      void set_adjacency_matrix(const TriangularView<Derived, StrictlyLower>& v, MatrixContents contents) { 
        adjacency_matrix = v; 
        lower_contents   = contents;
      }
      
      const dissimilarity_matrix_type& get_dissimilarity_matrix() const { return dissimilarity_matrix; }
      dissimilarity_matrix_type& get_dissimilarity_matrix() { return dissimilarity_matrix; }

      const distance_vector_type&  get_connectivity() const { return connectivity; }

      MatrixContents get_lower_matrix_contents() const { return lower_contents; }
      MatrixContents get_upper_matrix_contents() const { return upper_contents; }
      MatrixContents get_connectivity_contents() const { return connectivity_contents; }


      // Major workflow steps

      void update_correlation();
      void update_adjacency();
      void update_adjacency_with_powerfn(double beta);
      void update_connectivity();
      void update_dissimilarity();

      //Load a WGCNA session from a file
      int load_from_file(string file_name) {
        int file_version;
        
        //Open the file and prepare to read from it
        ifstream ifs(file_name.c_str(), ios::binary);
        
        //If file header doesn't look right, exit
        ifs.read((char *)&file_version, sizeof(file_version));
        if (file_version != FILE_VERSION) {
          return 1;
        }
        
        //Load data matrix
        int rows = 0;
        int cols = 0;
        ifs.read((char *)&rows, sizeof(rows));
        ifs.read((char *)&cols, sizeof(cols));
        double* data_tmp = (double*)malloc(sizeof(double)*rows*cols);
        ifs.read((char *)data_tmp, sizeof(double)*rows*cols);
        new (&data) data_matrix_type(data_tmp,rows,cols);
        
        //Load working matrix
        ifs.read((char *)&rows, sizeof(rows));
        ifs.read((char *)&cols, sizeof(cols));
        working_matrix.resize(rows, cols);
        ifs.read((char *)working_matrix.data(), sizeof(double)*rows*cols);
        
        //Set the adjacency and dissimilarity matrices as triangular views into the working matrix
        adjacency_matrix = working_matrix.template triangularView<StrictlyLower>();
        dissimilarity_matrix = working_matrix.template triangularView<StrictlyUpper>();

        //Load the connectivity vector
        ifs.read((char *)&rows, sizeof(rows));
        ifs.read((char *)&cols, sizeof(cols));
        connectivity.resize(rows, cols);
        ifs.read((char *)connectivity.data(), sizeof(double)*rows*cols);
        
        //Load the matrix contents
        ifs.read((char *)&lower_contents, sizeof(lower_contents));
        ifs.read((char *)&upper_contents, sizeof(upper_contents));
        ifs.read((char *)&connectivity_contents, sizeof(connectivity_contents));
         
        //Load the power adjacency function parameters, and set the power adjacency function
        bool narm;
        DistanceScalar beta;
        ifs.read((char *)&beta, sizeof(beta));
        ifs.read((char *)&narm, sizeof(narm));
        adjacency_function = PowerAdjacency(beta, narm);
        
        //Close the file and exit with no error
        ifs.close();
        return 0;
      }
      
      //Save a WGCNA session to a file
      int store_to_file(string file_name) {
        
        //Open the file and prepare to write to it
        ofstream ofs(file_name.c_str(), ios::binary);
        
        //Store the current file version as as the file header        
        int file_version = FILE_VERSION;
        ofs.write((char *)&file_version, sizeof(int));

        //Store the data matrix
        int rows = (int)get_data_matrix().rows();
        int cols = (int)get_data_matrix().cols();  
        ofs.write((char *)&rows, sizeof(rows));
        ofs.write((char *)&cols, sizeof(cols));
        ofs.write((char *)get_data_matrix().data(), get_data_matrix().size() * sizeof(double));
        
        //Store the working matrix
        rows = (int)get_working_matrix().rows();
        cols = (int)get_working_matrix().cols();  
        ofs.write((char *)&rows, sizeof(rows));
        ofs.write((char *)&cols, sizeof(cols));
        ofs.write((char *)get_working_matrix().data(), get_working_matrix().size() * sizeof(double));
        
        //Store the connectivity vector
        rows = (int)get_connectivity().rows();
        cols = (int)get_connectivity().cols();  
        ofs.write((char *)&rows, sizeof(rows));
        ofs.write((char *)&cols, sizeof(cols));
        ofs.write((char *)get_connectivity().data(), get_connectivity().size() * sizeof(double));

        //Store the matrix contents
        ofs.write((char *)&lower_contents, sizeof(lower_contents));
        ofs.write((char *)&upper_contents, sizeof(upper_contents));
        ofs.write((char *)&connectivity_contents, sizeof(connectivity_contents));
        
        //Store the power adjacency function parameters
        bool narm = adjacency_function.GetNarm();
        DistanceScalar beta = adjacency_function.GetBeta();
        ofs.write((char *)&beta, sizeof(beta));
        ofs.write((char *)&narm, sizeof(narm));
        
        //Close the file and exit without an error
        ofs.close();
        return 0;
      }

    private:

      // Input data -- Assumes cols are observations, i.e. mxn matrix
      //const data_matrix_type data;
      data_matrix_type data;
    
      // Core working matrix. We rarely use this directly, and instead acccess it
      // via specific triangular views for adjancecy, TOM dissimilarity, etc.
      distance_matrix_type working_matrix;
    
      adjacency_matrix_type     adjacency_matrix;
      dissimilarity_matrix_type dissimilarity_matrix;
      distance_vector_type      connectivity;
  
      PowerAdjacency adjacency_function;
  
      MatrixContents lower_contents, upper_contents, connectivity_contents;
  };


  namespace {

    template<class DistanceScalar, class Derived>
    DistanceScalar pairwise_complete_obs(const MatrixBase<Derived>& a, const MatrixBase<Derived>& b) {
      
      ssize_t pair_count = 0;
      
      DistanceScalar mean_a = 0., mean_b = 0.;
      for (ssize_t i=0; i<a.size(); i++) {
        if (!std::isnan(a(i)) && !std::isnan(b(i))) {
          mean_a += a(i);
          mean_b += b(i);
          pair_count++;
        }
      }
      
      if (pair_count == 0)
        return std::numeric_limits<DistanceScalar>::quiet_NaN();

      mean_a /= pair_count;
      mean_b /= pair_count;

      DistanceScalar num = 0., denom_a = 0., denom_b = 0.;
      for (ssize_t i=0; i<a.size(); i++) {
        if (!std::isnan(a(i)) && !std::isnan(b(i))) {
          num += (a(i) - mean_a) * (b(i) - mean_b);
          denom_a += (a(i) - mean_a) * (a(i) - mean_a);
          denom_b += (b(i) - mean_b) * (b(i) - mean_b);
        }
      }

      return num / std::sqrt(denom_a * denom_b);
    }

  } // anonymous


  template<class DataScalar, class DistanceScalar>
  void WGCNA<DataScalar, DistanceScalar>::update_correlation() {
    distance_row_matrix_type means = data.colwise().sum() / (DistanceScalar)data.rows();
    distance_matrix_type deviation = data - means.replicate(data.rows(),1);
    distance_row_matrix_type denom = deviation.colwise().norm();

    // Simulatenous computation of the correlation, and application of the power adjacency function
#ifdef _OPENMP
    // Note that class members cannot appear in visibility clauses, however, variables are shared
    // by default which is exactly what we want here for 'adjacency_matrix'
    #pragma omp parallel for shared(deviation, denom)
#endif
    for (ssize_t c=0; c<(observation_count()-1); c++) {
      for (ssize_t r=c+1; r<observation_count(); r++) {
        DistanceScalar cor =  deviation.col(r).dot(deviation.col(c)) / (denom(r) * denom(c));
        if (std::isnan(cor)) {
          // We must have missing data, attempt to obtain valid correlation via 'pairwise.complete.obs'
          // strategy. Note that NaNs are still posssible if one observation is all NaNs.
          cor = pairwise_complete_obs<DistanceScalar>(data.col(r), data.col(c));
        } 
        adjacency_matrix(r,c) = cor;
      }
    }
    
    lower_contents = CORRELATION;
  }


  template<class DataScalar, class DistanceScalar>
  void WGCNA<DataScalar, DistanceScalar>::update_adjacency_with_powerfn(double beta) {
    adjacency_function = PowerAdjacency(beta);
    update_adjacency();
  }

  template<class DataScalar, class DistanceScalar>
  void WGCNA<DataScalar, DistanceScalar>::update_adjacency() {
    if (lower_contents != CORRELATION)
      throw std::runtime_error("Lower triangle of working matrix not set to correlation");

#ifdef _OPENMP
    // Note that class members cannot appear in visibility clauses, however, variables are shared
    // by default which is exactly what we want here for 'working_matrix'
    #pragma omp parallel for 
#endif
    for (ssize_t c=0; c<(observation_count()-1); c++) {
      working_matrix.block(c+1, c, observation_count()-(c+1), 1) =
        working_matrix.block(c+1, c, observation_count()-(c+1), 1).unaryExpr(adjacency_function);
    }

    lower_contents = ADJACENCY;
  }

  template<class DataScalar, class DistanceScalar>
  void WGCNA<DataScalar, DistanceScalar>::update_connectivity() {
    if (lower_contents != ADJACENCY)
      throw std::runtime_error("Lower triangle of working matrix not set to adjacency");
    
    // Stay in strictly lower adjancency matrix
    for (ssize_t i=0; i<observation_count(); i++) {
      connectivity(i) = working_matrix.row(i).head(i).sum() + working_matrix.col(i).tail(observation_count()-(i+1)).sum(); 
    }

    connectivity_contents = CONNECTIVITY;
  }

  template<class DataScalar, class DistanceScalar>
  void WGCNA<DataScalar, DistanceScalar>::update_dissimilarity() {
    if (lower_contents != ADJACENCY || connectivity_contents != CONNECTIVITY)
      throw std::runtime_error("Adjancency (lower triangle of working matrix) and connectivity not available");
  
    // Set diagonal to 1.0 before computing TOM 
    working_matrix.diagonal() = distance_vector_type::Ones(observation_count());  

    // Blocked TOM computation
    // We use a block size generally smaller than input matrix to keep our memory footprint O(n^2) with a constant close
    // to 1 (for large matrices). The blocking also creates opportunities for coarse grain parallelism.
    const ssize_t block_size = 100;
  
#ifdef _OPENMP
    #pragma omp parallel
#endif
    {

    distance_matrix_type row_block(block_size, observation_count()), col_block(observation_count(), block_size), l_ij(block_size, block_size);
  
#ifdef _OPENMP
    #pragma omp for
#endif
    for (ssize_t cb=0; cb<observation_count(); cb+=block_size) {
      
      ssize_t c_span = std::min(block_size, observation_count()-cb);  // Actual width of column "block"

      // Construct "column" block using only strictly lower (adjacency) portion of working matrix
      
      // Since we working from triagonal matrix, we have to copy three blocks:
      //  1. Row portion to the diagonal
      //  2. Column portion from the diagonal
      //  3. Copy triangular portion at the diagonal to its transpose
      
      col_block.block(0, 0, cb, c_span) = working_matrix.block(cb, 0, c_span, cb).transpose();
      col_block.block(cb, 0, observation_count()-cb, c_span) = working_matrix.block(cb, cb, observation_count()-cb, c_span);
      col_block.block(cb, 0, c_span, c_span).template triangularView<StrictlyUpper>() = 
        col_block.block(cb, 0, c_span, c_span).template triangularView<StrictlyLower>().transpose();

      for (ssize_t rb=cb; rb<observation_count(); rb+=block_size) {
      
        ssize_t r_span = std::min(block_size, observation_count()-rb);
      
        // Construct "row" block using only strictly lower (adjacency) portion of working matrix
        row_block.block(0, 0, r_span, rb) = working_matrix.block(rb, 0, r_span, rb);
        row_block.block(0, rb, r_span, observation_count()-rb) = working_matrix.block(rb, rb, observation_count()-rb, r_span).transpose();
        row_block.block(0, rb, r_span, r_span).template triangularView<StrictlyLower>() = 
          row_block.block(0, rb, r_span, r_span).template triangularView<StrictlyUpper>().transpose();

        l_ij = row_block * col_block; 

        for (ssize_t c=cb; c<std::min(observation_count()-1, cb+c_span); c++) {
          for (ssize_t r=std::max(rb, c+1); r<(rb+r_span); r++) {

            // Equation (4) in "Statistical Applications in Genetics..."
            // Despite what equation (6) suggests, by virtue of using matrix multiply to compute l_ij, the reuslting value is too large
            // by 2 * a_ij. As a result we substract a_ij in the numerator, instead of add it as shown in equation 4 in the paper.
            dissimilarity_matrix(c, r) = 1. - 
              ((l_ij(r-rb,c-cb) - adjacency_matrix(r, c)) / (std::min(connectivity(r), connectivity(c)) + 1. - adjacency_matrix(r, c)));
          }
        } // inner
      }
    } // block
    
    } // openmp

    upper_contents = DISSIMILARITY;
  }


} // Coexpp


#endif
