#ifdef _OPENMP
#include <omp.h>
#endif

#include "coexpp.h"
#include <string>

RcppExport SEXP coexpp_set_num_threads(SEXP threads) {
BEGIN_RCPP
#ifdef _OPENMP
  omp_set_num_threads(Rcpp::as<int>(threads));
  return Rcpp::wrap(omp_get_max_threads());
#else
  return Rcpp::wrap(1L);
#endif
END_RCPP
}


namespace {
  typedef Coexpp::WGCNA<double, double> NumericWGCNA;
}

RcppExport SEXP cluster(NumericWGCNA* wgcna, Rclusterpp::LinkageKinds linkage) {
BEGIN_RCPP
  using namespace Rcpp;
  using namespace Rclusterpp;
  using namespace Coexpp;
  
  // Clustering using average link
  typedef NumericCluster::plain cluster_type;
  ClusterVector<cluster_type> clusters(wgcna->observation_count());

  // Clustering from distance matrix is destructive, but we don't want to create additional
  // large temporaries so we are going to write over, then rebuld, the adjacency matrix during
  // the clustering process.
  wgcna->set_adjacency_matrix(wgcna->get_dissimilarity_matrix().transpose(), NumericWGCNA::DISSIMILARITY);

  init_clusters(wgcna->get_adjacency_matrix(), clusters);

  switch (linkage) {
  default:
    throw std::invalid_argument("Linkage method not supported");
  case Rclusterpp::AVERAGE:
    cluster_via_rnn( average_link<cluster_type>(wgcna->get_adjacency_matrix(), FromDistance), clusters );
    break;
  case Rclusterpp::SINGLE:
    cluster_via_rnn( single_link<cluster_type>(wgcna->get_adjacency_matrix(), FromDistance),  clusters );
    break;
  case Rclusterpp::COMPLETE:
    cluster_via_rnn( complete_link<cluster_type>(wgcna->get_adjacency_matrix(), FromDistance), clusters );
    break;  
  }

  wgcna->update_correlation();
  wgcna->update_adjacency();  // Recompute adjacency after clustering
  return wrap( clusters );

END_RCPP
}

RcppExport SEXP k_from_correlation(NumericWGCNA* wgcna, double beta) {
BEGIN_RCPP
  using namespace Rcpp;
  using namespace Rclusterpp;
  using namespace Coexpp;

  if (wgcna->get_lower_matrix_contents() != NumericWGCNA::CORRELATION) {
    throw std::runtime_error("Lower triangle of working matrix not set to correlation");
  }

  NumericWGCNA::distance_vector_type k(wgcna->observation_count());
  NumericWGCNA::PowerAdjacency op = NumericWGCNA::PowerAdjacency(beta, true);
  
  // Stay in strictly lower correlation matrix
#ifdef _OPENMP
  #pragma omp parallel for shared(wgcna, k, op)
#endif
  for (ssize_t i=0; i<wgcna->observation_count(); i++) {
    k(i) = 
      wgcna->get_working_matrix().row(i).head(i).unaryExpr(op).sum() + 
      wgcna->get_working_matrix().col(i).tail(wgcna->observation_count()-(i+1)).unaryExpr(op).sum(); 
  }
  return wrap(k);

END_RCPP
}

namespace {

  static const int Row = 0, Col = 1;

  template<class Vector, class Scalar, class Index, int Order>
  struct ModuleKVisitor {
    
    const Vector& assignments;
    int module;
    double k;
    
    ModuleKVisitor(const Vector& assignments_, int module_, double k_ = 0.) : assignments(assignments_), module(module_), k(k_) {}  
    
    void init(const Scalar& value, Index i, Index j) {
      if (assignments.size() == 0)  // init called, even if visited vector is zero length...
        return;
      if (assignments(Order == Row ? j : i) == module)  // Recall 'visiting' row involves constant row, changing columns
        k += value;
    }
    
    void operator() (const Scalar& value, Index i, Index j) {
      if (assignments(Order == Row ? j : i) == module)
        k += value;
    }
  };
  

} // anonymous


#define KVISITOR( ORDER ) ModuleKVisitor<\
  Eigen::Map<Eigen::VectorXi>::ConstSegmentReturnType,\
  NumericWGCNA::distance_scalar_type,\
  NumericWGCNA::distance_matrix_type::Index,\
  ORDER\
> 


RcppExport SEXP k_by_module(NumericWGCNA* wgcna, const Eigen::Map<Eigen::VectorXi>& mod_assign) {
BEGIN_RCPP
  using namespace Rcpp;
  using namespace Rclusterpp;
  using namespace Coexpp;

  if (wgcna->get_lower_matrix_contents() != NumericWGCNA::ADJACENCY) {
    throw std::runtime_error("Lower triangle of working matrix not set to adjancency");
  }

  NumericWGCNA::distance_vector_type k(wgcna->observation_count());
  
  // Stay in strictly lower adjancency matrix
#ifdef _OPENMP
  #pragma omp parallel for shared(wgcna, k)  // mod_assign default shared
#endif
  for (ssize_t i=0; i<wgcna->observation_count(); i++) {
  
    double k_ = 0.;
    {
      KVISITOR(Row) v(mod_assign.head(i), mod_assign(i)); 
      wgcna->get_working_matrix().row(i).head(i).visit(v);
      k_ += v.k;
    }
    {
      KVISITOR(Col) v(mod_assign.tail(wgcna->observation_count()-(i+1)), mod_assign(i));  
      wgcna->get_working_matrix().col(i).tail(wgcna->observation_count()-(i+1)).visit(v);
      k_ += v.k;
    }
    k(i) = k_;
    
  }

  return wrap(k);

END_RCPP
}


// sumTOM is effectively computing 'k', but using TOM, instead of adjacency as the input
////

RcppExport SEXP sumTOM(NumericWGCNA* wgcna) {
  BEGIN_RCPP
  using namespace Rcpp;
  using namespace Rclusterpp;
  using namespace Coexpp;

  if (wgcna->get_upper_matrix_contents() != NumericWGCNA::DISSIMILARITY) {
    throw std::runtime_error("Lower triangle of working matrix not set to TOM dissimilarity");
  }

  NumericWGCNA::distance_vector_type k(wgcna->observation_count());

  // Stay in strictly upper TOM matrix
#ifdef _OPENMP
  #pragma omp parallel for shared(wgcna, k)  // mod_assign default shared
#endif
  for (ssize_t i=0; i<wgcna->observation_count(); i++) {
    k(i) = 
      wgcna->get_working_matrix().col(i).head(i).sum() +  
      wgcna->get_working_matrix().row(i).tail(wgcna->observation_count()-(i+1)).sum();
  }

  return wrap(k);

END_RCPP
}

RcppExport SEXP sumTOM_by_module(NumericWGCNA* wgcna, const Eigen::Map<Eigen::VectorXi>& mod_assign) {
BEGIN_RCPP
  using namespace Rcpp;
  using namespace Rclusterpp;
  using namespace Coexpp;

  if (wgcna->get_upper_matrix_contents() != NumericWGCNA::DISSIMILARITY) {
    throw std::runtime_error("Lower triangle of working matrix not set to TOM dissimilarity");
  }

  NumericWGCNA::distance_vector_type k(wgcna->observation_count());
  
  // Stay in strictly upper TOM matrix
#ifdef _OPENMP
  #pragma omp parallel for shared(wgcna, k)  // mod_assign default shared
#endif
  for (ssize_t i=0; i<wgcna->observation_count(); i++) {
  
    double k_ = 0.;
    {
      KVISITOR(Col) v(mod_assign.head(i), mod_assign(i)); 
      wgcna->get_working_matrix().col(i).head(i).visit(v);
      k_ += v.k;
    }
    {
      KVISITOR(Row) v(mod_assign.tail(wgcna->observation_count()-(i+1)), mod_assign(i));  
      wgcna->get_working_matrix().row(i).tail(wgcna->observation_count()-(i+1)).visit(v);
      k_ += v.k;
    }
    k(i) = k_;
    
  }

  return wrap(k);

END_RCPP
}


#undef KVISITOR

namespace {

  template <class OutV, class InV, class AssgnV>
  struct ModuleCCVisitor {
    typedef typename InV::Scalar Scalar;
    typedef typename InV::Index Index;

    OutV condense;
    const AssgnV& assignments;
    int module;
    ssize_t c_idx;

    ModuleCCVisitor(OutV condense_, const AssgnV& assignments_, int module_) : 
      condense(condense_), assignments(assignments_), module(module_), c_idx(0) {}  
    
    void init(const Scalar& value, Index i, Index j) {
      if (assignments(i) == module)
        condense(c_idx++) = value;
    }
    
    void operator() (const Scalar& value, Index i, Index j) {
      if (assignments(i) == module)
        condense(c_idx++) = value;
    }

  };

} // anonymous


#define CCVISITOR ModuleCCVisitor<\
  NumericWGCNA::distance_matrix_type::RowXpr,\
  NumericWGCNA::distance_vector_type,\
  Eigen::Map<Eigen::VectorXi> >


RcppExport SEXP cc_by_module(NumericWGCNA* wgcna, const Eigen::Map<Eigen::VectorXi>& mod_assign, const Eigen::Map<Eigen::VectorXi>& mod_count) {
BEGIN_RCPP
  using namespace Rcpp;
  using namespace Rclusterpp;
  using namespace Coexpp;

  if (wgcna->get_lower_matrix_contents() != NumericWGCNA::ADJACENCY) {
    throw std::runtime_error("Lower triangle of working matrix not set to adjacency");
  }

  NumericWGCNA::distance_vector_type cc(wgcna->observation_count()), av(wgcna->observation_count());
  
  cc.setZero();

  for (ssize_t i=0; i<mod_count.size(); i++) {  // foreach module

    if (mod_count(i) <= 1)
      continue;  // Degenerate case

    // Condensed adjacency matrix for module
    NumericWGCNA::distance_matrix_type adj(mod_count(i), mod_count(i));

    // Construct condensed matrix from strictly lower triangular adjacency matrix
    for (ssize_t o=0, s=0; o<wgcna->observation_count(); o++) {
      if (mod_assign(o) == (i+1)) {  // Recall R is 1-indexed, thus assignments start at 1...
        av.head(o) = wgcna->get_working_matrix().row(o).head(o);
        av.tail(wgcna->observation_count()-(o+1)) = wgcna->get_working_matrix().col(o).tail(wgcna->observation_count()-(o+1));
        
        CCVISITOR v(adj.row(s++), mod_assign, i+1);
        av.visit( v );
      } 
    }
    
    adj.diagonal() = NumericWGCNA::distance_vector_type::Zero(mod_count(i));  
          
    {  // Compute the clustering coefficient
      NumericWGCNA::distance_row_matrix_type first = adj.colwise().sum(), result;
      
      result = (adj * adj).cwiseProduct(adj).colwise().sum().cwiseQuotient(  // Equation 8 in "Statistical Applications..."
        first.cwiseProduct(first) - adj.cwiseProduct(adj).colwise().sum()  // Equation 11 
      );

      for (ssize_t o=0, s=0; o<wgcna->observation_count(); o++) {
        if (mod_assign(o) == (i+1))  // 'Decondense' clustering coefficient
          cc(o) = result(s++);
      }
    }
  }
  
  return wrap(cc);

END_RCPP
}

#undef CCVISITOR

RcppExport SEXP sample_tom_matrix(NumericWGCNA* wgcna, const Eigen::Map<Eigen::VectorXi>& samples) {
BEGIN_RCPP
  using namespace Rcpp;
  using namespace Rclusterpp;
  using namespace Coexpp;

  if (wgcna->get_upper_matrix_contents() != NumericWGCNA::DISSIMILARITY) {
    throw std::runtime_error("Upper triangle of working matrix not set to TOM dissimilarity");
  }

  // We create two temporaries for the two phases -- columns then rows -- of the sampling 
  NumericWGCNA::distance_matrix_type 
    sampled_col(wgcna->observation_count(), samples.size()),
    sampled_row(samples.size(), samples.size());
    

  for (ssize_t i=0; i<samples.size(); i++) {
    ssize_t idx = samples(i) - 1; // Recall R is 1-indexed, and thus so is samples
    sampled_col.col(i).head(idx) = wgcna->get_working_matrix().col(idx).head(idx);
    sampled_col.col(i).tail(wgcna->observation_count()-(idx+1)) = wgcna->get_working_matrix().row(idx).tail(wgcna->observation_count()-(idx+1));
  }
  for (ssize_t i=0; i<samples.size(); i++)
    sampled_row.row(i) = sampled_col.row(samples(i)-1);

  return wrap(sampled_row);

END_RCPP
}

RcppExport SEXP sample_adj_matrix(NumericWGCNA* wgcna, const Eigen::Map<Eigen::VectorXi>& samples) {
BEGIN_RCPP
  using namespace Rcpp;
  using namespace Rclusterpp;
  using namespace Coexpp;

  if (wgcna->get_lower_matrix_contents() != NumericWGCNA::ADJACENCY) {
    throw std::runtime_error("Lower triangle of working matrix not set to adjacency");
  }

  // We create two temporaries for the two phases -- columns then rows -- of the sampling 
  NumericWGCNA::distance_matrix_type 
    sampled_col(wgcna->observation_count(), samples.size()),
    sampled_row(samples.size(), samples.size());
    

  for (ssize_t i=0; i<samples.size(); i++) {
    ssize_t idx = samples(i) - 1; // Recall R is 1-indexed, and thus so is samples
    sampled_col.col(i).head(idx) = wgcna->get_working_matrix().row(idx).head(idx);
    sampled_col.col(i).tail(wgcna->observation_count()-(idx+1)) = wgcna->get_working_matrix().col(idx).tail(wgcna->observation_count()-(idx+1));
  }
  for (ssize_t i=0; i<samples.size(); i++)
    sampled_row.row(i) = sampled_col.row(samples(i)-1);

  return wrap(sampled_row);

END_RCPP
}

RcppExport SEXP get_observation_count(NumericWGCNA* wgcna) {
BEGIN_RCPP
  // Rcpp does not handle ssize_t (long log int) on x86_64, so force
  // static cast to int
  return Rcpp::wrap(static_cast<int>(wgcna->observation_count()));
END_RCPP
}

//Store WGCNA data to a file
RcppExport SEXP store_wgcna_session(NumericWGCNA* wgcna, string file_name) {
BEGIN_RCPP

  return Rcpp::wrap(wgcna->store_to_file(file_name));

END_RCPP
}

//Store WGCNA data from a file
RcppExport SEXP load_wgcna_session(NumericWGCNA* wgcna, string file_name) {
BEGIN_RCPP
  
  return Rcpp::wrap(wgcna->load_from_file(file_name));
  
END_RCPP
}

//Get lower matrix contents
RcppExport SEXP get_lower_matrix_contents_int(NumericWGCNA* wgcna) {
BEGIN_RCPP

  return Rcpp::wrap((int)wgcna->get_lower_matrix_contents());

END_RCPP
}

//Get upper matrix contents
RcppExport SEXP get_upper_matrix_contents_int(NumericWGCNA* wgcna) {
BEGIN_RCPP

  return Rcpp::wrap((int)wgcna->get_upper_matrix_contents());

END_RCPP
}

//Get lower matrix contents
RcppExport SEXP get_connectivity_contents_int(NumericWGCNA* wgcna) {
BEGIN_RCPP

  return Rcpp::wrap((int)wgcna->get_connectivity_contents());

END_RCPP
}

RCPP_MODULE(mod_coexpp) {
  using namespace Rcpp;
  using namespace Coexpp;
    
  class_<NumericWGCNA>( "Wgcna" )
  
    // expose the default constructor
    .constructor<NumericWGCNA::data_matrix_type>()
    
    // data getters 
    .method( "observation_count", &get_observation_count, "get observation count")
    .const_method( "data", &NumericWGCNA::get_data_matrix, "get data matrix" )
    .const_method( "working_matrix", &NumericWGCNA::get_working_matrix, "get working matrix (note it might be very large)" )
    .const_method( "connectivity", &NumericWGCNA::get_connectivity, "get connectivity vector" )
    .method( "lower_contents", &get_lower_matrix_contents_int, "get matrix lower contents" )
    .method( "upper_contents", &get_upper_matrix_contents_int, "get matrix upper contents" )
    .method( "connectivity_contents", &get_connectivity_contents_int, "get connectivity contents" )
        
    // workflow methods
    .method( "compute_correlation", &NumericWGCNA::update_correlation, "compute correlation matrix from data")
    .method( "compute_adjacency", &NumericWGCNA::update_adjacency, "compute adjacency from data with stored adjacency function" )
    .method( "compute_adjacency_with_powerfn", &NumericWGCNA::update_adjacency_with_powerfn, "compute adjacency from data with powerfn" )
    .method( "compute_connectivty", &NumericWGCNA::update_connectivity, "compute connectivity from adjacency" )
    .method( "compute_dissimilarity", &NumericWGCNA::update_dissimilarity, "compute dissimilarity from adjacency and connectivity" )
    .method( "cluster", &cluster, "hierarchical clustering using dissimilarity matrix")
    .method( "k_from_corr", &k_from_correlation, "compute k from correlation")
    // module statistics
    .method( "k_by_module", &k_by_module, "compute k by/within modules")
    .method( "sumTOM", &sumTOM, "sum TOM entries by gene")
    .method( "sumTOM_by_module", &sumTOM_by_module, "sum TOM entries by/within modules")
    .method( "cc_by_module", &cc_by_module, "compute clustering coefficient by/within modules")
    // utilities
    .method( "sampleTOMMatrix", &sample_tom_matrix, "sample TOM matrix")
    .method( "sampleAdjMatrix", &sample_adj_matrix, "sample adjacency matrix")
    
    //Store and load WGCNA session
    .method( "store", &store_wgcna_session, "store wgcna session")
    .method( "load", &load_wgcna_session, "load wgcna session")
  ;
}
