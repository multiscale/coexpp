\name{coexppSetThreads}
\alias{coexppSetThreads}
\title{Set maximimum number of threads to be used by coexpp}
\usage{
  coexppSetThreads(threads = 1)
}
\arguments{
  \item{threads}{Number of threads. If \code{NULL}, threads
  will be set to number of cores.}
}
\value{
  Set number of threads
}
\description{
  Set maximimum number of threads to be used by coexpp
}

