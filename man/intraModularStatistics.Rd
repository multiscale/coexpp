\name{intraModularStatistics}
\alias{intraModularStatistics}
\title{Compute intra-modular statistics for color labeled genes}
\usage{
  intraModularStatistics(clusters, modules,
    stats = c("connectivity", "topological"))
}
\arguments{
  \item{clusters}{Instance of \code{CoexppClusters} with
  coexpression analysis results}

  \item{modules}{Factor of module assignments for all
  observations}

  \item{stats}{A subset of "connectivity", "topological",
  "clustering" that indicates kinds of statistics to be
  computed. Note, that "clustering" or specifically the
  clustering coefficient can be a very slow computation and
  is thus skipped by default.}
}
\description{
  Compute intra-modular statistics for color labeled genes
}

